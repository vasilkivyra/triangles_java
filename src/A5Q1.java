import java.awt.*;

class A5Q1 {

    private static void drawTriangles(int level, double x_left, double y_left, double current_side_len) {
        drawTriangles(level, x_left, y_left, current_side_len, Color.BLACK);
    }

    private static void drawTriangles(int level, double x_left, double y_left, double side_length, java.awt.Color color) {

        if (level == 0) return;

        double x_right, y_right, x_top, y_top;

        if (color == Color.WHITE) {
            x_right = x_left + side_length;
            y_right = y_left;
            x_top = x_left + side_length / 2;
            y_top = y_left - (Math.sqrt(3)) * side_length / 2;

        } else {
            x_right = x_left + side_length;
            y_right = y_left;
            x_top = x_left + side_length / 2;
            y_top = y_left + (Math.sqrt(3)) * side_length / 2;
        }

        double[] x_polygon = {x_left, x_right, x_top};
        double[] y_polygon = {y_left, y_right, y_top};

        // draw the triangle
        StdDraw.setPenColor(color);
        StdDraw.filledPolygon(x_polygon, y_polygon);


        // draw sub triangles
        int previous_level = level - 1;
        double sub_side_length = side_length / 2;


        //----------------------------------------------------------//
        // a recursive call for the first sub triangle
        double first_sub_left_x = x_left;
        double first_sub_left_y = y_left;

        drawTriangles(previous_level, first_sub_left_x, first_sub_left_y, sub_side_length, Color.GREEN);


        //----------------------------------------------------------//
        // a recursive call for the second sub triangle
        double second_sub_left_x = (x_left + x_right) / 2;
        double second_sub_left_y = (y_left + y_right) / 2;

        drawTriangles(previous_level, second_sub_left_x, second_sub_left_y, sub_side_length, Color.BLUE);


        //----------------------------------------------------------//
        // a recursive call for the third sub triangle
        double third_sub_left_x = (x_left + x_top) / 2;
        double third_sub_left_y = (y_left + y_top) / 2;

        drawTriangles(previous_level, third_sub_left_x, third_sub_left_y, sub_side_length, Color.BLACK);


        //----------------------------------------------------------//
        // a recursive call for the fourth WHITE sub triangle
        double white_sub_left_x = third_sub_left_x;
        double white_sub_left_y = third_sub_left_y;

        drawTriangles(previous_level, white_sub_left_x, white_sub_left_y, sub_side_length, Color.WHITE);
    }


    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        double x_left = Double.parseDouble(args[1]);
        double y_left = Double.parseDouble(args[2]);
        double cur_side_len = Double.parseDouble(args[3]);

        drawTriangles(n, x_left, y_left, cur_side_len);
    }
}


/*
command line command to compile and run the class:

java A5Q1 5 0.0 0.0 0.0

 */